## Tests

#### Step One:

You should create a database with mysql and laravel migrations system.
We want a hospital application. The front api will be send this json;
```
Create a doctor
{
"full_name": "Erdem Onur",
"birth_day": "12.04.1986",
"department": "Cardiologist"
}
```
You should do doctor delete and update process with route system
Create a medicine
```
{
"name": "Parol",
}
```
You should do doctor delete and update process with route system
Create new patient (Patients who haven’t recovered should not be able to re-register)
```
{
"doctor_id": 1,
"patient_id_number": 14869933800,
"patient_full_name": "Behzat Cozer",
"patient_gender": "M",
"medicines": [1,3,4,5],
"recovered": false
}
```
You should do patient delete and update process with route system. And they just can
update recovered columns.

We don’t want any authentications system. You should just create, update and delete
process with laravel.
#### Step Two
You should do filter system for patients. They should can search with patient name, docktor
name or medicine name. You should give a response http status codes with laravel api
resources system.

Filter url example: /api/patients?doctor_name=Erdem%20Onur


### Installation instructions

- **git clone https://gitlab.com/valeriyagodlevskaya/hospital-api-laravel.git**
- **cd hospital-api-laravel**
- **composer install**
- **create .env file, copy content from .env.example file. Change .env to your DB settings**
- **php artisan key:generate**
- **php artisan migrate**

### REST API

#### Create a doctor

##### Request
```
POST /api/v1/doctors/

{
    "full_name":"Erdem Onur",
    "birth_day":"12.04.1986",
    "department":"Cardiologist"
}
```

##### Response
```
{
    "data": {
        "full_name": "Erdem Onur",
        "birth_day": "1986-04-12",
        "department": "Cardiologist",
        "id": 5
    }
}
```
#### Update doctor

##### Request
```
GET /api/v1/doctors/{id}/update

{
    "full_name":"Erdem Onur",
    "birth_day":"14.04.1986",
    "department":"Cardiologist"
}
```

##### Response
```
{
    "data": {
        "id": 5,
        "full_name": "Erdem",
        "birth_day": "1986-04-14",
        "department": "Cardiologist"
    }
}
```
#### Delete doctor

##### Request
```
GET /api/v1/doctors/{id}/delete
```

##### Response
```
{}
```
#### Create a medicine

##### Request
```
POST /api/v1/medicines/
```

##### Response
```
{
    "data": {
        "name": "Parol",
    }
}
```
#### Update medicine

##### Request
```
GET /api/v1/medicines/{id}/update
```

##### Response
```
{
    "data": {
        "id": 1,
        "name": "Parol",
    }
}
```
#### Delete medicine

##### Request
```
GET /api/v1/medidicnes/{id}/delete
```

##### Response
```
{}
```
#### Create a patients

##### Request
```
POST /api/v1/patients/

{
    "doctor_id":3,
    "patient_id_number":8888,
    "patient_full_name":"Lera",
    "patient_gender":"M",
    "medicines":[4],
    "recovered":false
}
```

##### Response
```
{
    "data": [
        {
            "patient_id_number": 14869933800,
            "doctor": {
                "id": 3,
                "full_name": "Erdem",
                "birth_day": "1986-04-14",
                "department": "Cardiologist"
            },
            "full_name": "Lera",
            "gender": "M",
            "recovered": false,
            "medicines": [
                {
                    "id": 4,
                    "name": "Test2",
                    "pivot": {
                        "patient_id": 34,
                        "medicine_id": 4
                    }
                }
            ]
        }
    ]
}
```
#### Update patient

##### Request
```
GET /api/v1/patients/{id}/update

{
    "doctor_id":3,
    "patient_id_number":14869933800,
    "patient_full_name":"Lera",
    "patient_gender":"M",
    "medicines":[4],
    "recovered":true
}
```

##### Response
```
{
    "data": [
        {
            "patient_id_number": 14869933800,
            "doctor": {
                "id": 3,
                "full_name": "Erdem",
                "birth_day": "1986-04-14",
                "department": "Cardiologist"
            },
            "full_name": "Lera",
            "gender": "M",
            "recovered": true,
            "medicines": [
                {
                    "id": 4,
                    "name": "Test2",
                    "pivot": {
                        "patient_id": 34,
                        "medicine_id": 4
                    }
                }
            ]
        }
    ]
}
```
#### Delete patient

##### Request
```
GET /api/v1/patients/{id}/delete
```

##### Response
```
{}
```
#### Filter patients

```
GET api/v1/patients?patient_name=Lera&doctor_name=Erdem&medicine_name=Test
```
##### Response
```
{
    "data": [
        {
            "full_name": "Lera"
        }
    ]
}
```


