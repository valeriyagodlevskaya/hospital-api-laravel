<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\DoctorRequest;
use App\Http\Services\Api\V1\DoctorService;
use Illuminate\Http\Response;

/**
 * Class DoctorController
 * @package App\Http\Controllers\Api\V1
 */
class DoctorController extends Controller
{
    /**
     * @var DoctorService
     */
    private $docService;

    /**
     * DoctorController constructor.
     * @param DoctorService $docService
     */
    public function __construct(DoctorService $docService)
    {
        $this->docService = $docService;
    }

    /**
     * @param DoctorRequest $doctorRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(DoctorRequest $doctorRequest)
    {
        try {
            $response = $this->docService->save($doctorRequest->all());

            return response()->json(['data' => $response], Response::HTTP_CREATED);
        }catch (\Exception $exception){

            return response()->json(['message' => $exception->getMessage()], Response::HTTP_NOT_FOUND);
        }

    }

    /**
     * @param int $id
     * @param DoctorRequest $doctorRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(int $id, DoctorRequest $doctorRequest)
    {
        try {
            $response = $this->docService->update($id, $doctorRequest->all());

            return response()->json(['data' => $response], Response::HTTP_OK);
        }catch (\Exception $exception){

            return response()->json(['message' => $exception->getMessage()], Response::HTTP_NOT_FOUND);
        }


    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(int $id)
    {
        try {
            $this->docService->destroy($id);

            return response()->json(null, Response::HTTP_OK);
        }catch (\Exception $exception){

            return response()->json(['message' => $exception->getMessage()], Response::HTTP_NOT_FOUND);
        }

    }

}
