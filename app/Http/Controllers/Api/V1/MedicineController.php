<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\MedicineRequest;
use App\Http\Services\Api\V1\MedicineService;
use Illuminate\Http\Response;

/**
 * Class MedicineController
 * @package App\Http\Controllers\Api\V1
 */
class MedicineController extends Controller
{
    /**
     * @var MedicineService
     */
    private $medicineService;

    /**
     * MedicineController constructor.
     * @param MedicineService $medicineService
     */
    public function __construct(MedicineService $medicineService)
    {
        $this->medicineService = $medicineService;
    }

    /**
     * @param MedicineRequest $medicineRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(MedicineRequest $medicineRequest)
    {
        try {
            $response = $this->medicineService->save($medicineRequest->all());

            return response()->json(['data' => $response], Response::HTTP_CREATED);
        }catch (\Exception $exception){

            return response()->json(['error' => $exception->getMessage()], Response::HTTP_NOT_FOUND);
        }

    }

    /**
     * @param int $id
     * @param MedicineRequest $medicineRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(int $id, MedicineRequest $medicineRequest)
    {
        try {
            $response = $this->medicineService->update($id, $medicineRequest->all());

            return response()->json(['data' => $response], Response::HTTP_OK) ;
        }catch (\Exception $exception){

            return response()->json(['error' => $exception->getMessage()], Response::HTTP_NOT_FOUND);
        }

    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(int $id)
    {
        try {
            $this->medicineService->destroy($id);

            return response()->json(null, Response::HTTP_OK);
        }catch (\Exception $exception){

            return response()->json(['error' => $exception->getMessage()], Response::HTTP_NOT_FOUND);
        }

    }


}
