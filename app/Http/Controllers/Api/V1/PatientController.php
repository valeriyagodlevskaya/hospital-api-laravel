<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\PatientRequest;
use App\Http\Services\Api\V1\PatientService;
use App\Models\Patient;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PatientController extends Controller
{

    /**
     * @var PatientService
     */
    private $patientService;

    /**
     * PatientController constructor.
     * @param PatientService $patientService
     */
    public function __construct(PatientService $patientService)
    {
        $this->patientService = $patientService;
    }

    /**
     * @param Request $request
     * @param Patient $patient
     * @return \Illuminate\Http\JsonResponse
     */
    public function filter(Request $request, Patient $patient)
    {
        try {
            $response = $this->patientService->searchByParams($request->only('patient_name', 'doctor_name'), $patient);

            return response()->json(['data' => $response], Response::HTTP_OK);
        }catch (\Exception $exception){

            return response()->json(['error' => $exception->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * @param PatientRequest $patientRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(PatientRequest $patientRequest)
    {
        try {
            $response = $this->patientService->create($patientRequest->all());

            return response()->json(['data' => $response], Response::HTTP_CREATED);
        }catch (\Exception $exception){

            return response()->json(['error' => $exception->getMessage()], Response::HTTP_NOT_FOUND);
        }

    }

    /**
     * @param int $id
     * @param PatientRequest $patientRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(int $id, PatientRequest $patientRequest)
    {
        try {
            $patient = $this->patientService->update($id, $patientRequest->all());

            return response()->json(['data' => $patient], Response::HTTP_ACCEPTED);
        }catch (\Exception $exception){

            return response()->json(['error' => $exception->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(int $id)
    {
        try {
            $this->patientService->destroy($id);

            return response()->json(null, Response::HTTP_NO_CONTENT);
        }catch (\Exception $exception){

            return response()->json(['error' => $exception->getMessage()], Response::HTTP_NOT_FOUND);
        }
    }

}
