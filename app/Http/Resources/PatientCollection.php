<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PatientCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'patient_id_number' => $this->patient_id_number,
            'doctor' => $this->doctor,
            'full_name' => $this->full_name,
            'gender' => $this->gender,
            'recovered' => $this->recovered,
            'medicines' => $this->medicines
        ];
    }
}
