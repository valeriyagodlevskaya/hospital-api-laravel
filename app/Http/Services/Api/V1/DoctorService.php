<?php

namespace App\Http\Services\Api\V1;

use App\Models\Doctor;
use Carbon\Carbon;

class DoctorService
{

    /**
     * @param array $params
     * @return Doctor
     */
    public function save(array $params)
    {
        if (empty($params)){
            throw new \DomainException('Can\'t empty params!');
        }

        $params['birth_day'] = $this->changeDateFormat($params['birth_day']);
        $doctor = new Doctor($params);

        if (!$doctor->save()){
            throw new \DomainException('An error occurred while saving doctor the record.');
        }

        return $doctor;
    }

    /**
     * @param int $id
     * @param array $params
     * @return Doctor
     */
    public function update(int $id, array $params)
    {
        $doctorObject = Doctor::findOrFail($id);
        $doctorObject->full_name = $params['full_name'];
        $doctorObject->birth_day = $this->changeDateFormat($params['birth_day']);
        $doctorObject->department = $params['department'];

        if (!$doctorObject->save()){
            throw new \DomainException('An error occurred while updating doctor the record.');
        }

        return $doctorObject;
    }

    /**
     * @param int $id
     */
    public function destroy(int $id)
    {
        $doctorObject = Doctor::findOrFail($id);

        if (!$doctorObject->delete()){
            throw new \DomainException('An error occurred while delete doctor.');
        }
    }

    /**
     * @param $date
     * @return string
     */
    private function changeDateFormat($date)
    {
        return Carbon::createFromFormat('d.m.Y', $date)->format('Y-m-d');
    }


}
