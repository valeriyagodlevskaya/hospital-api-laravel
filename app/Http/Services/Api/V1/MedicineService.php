<?php

namespace App\Http\Services\Api\V1;

use App\Models\Medicine;

/**
 * Class MedicineService
 * @package App\Http\Services\Api\V1
 */
class MedicineService
{
    /**
     * @param array $medicineData
     * @return Medicine
     */
    public function save(array $medicineData)
    {
        $medicine = new Medicine($medicineData);

        if (!$medicine->save()){
            throw new \DomainException('An error occurred while saving medicine the record.');
        }

        return $medicine;
    }

    /**
     * @param int $id
     * @param array $medicineData
     * @return Medicine
     */
    public function update(int $id, array $medicineData)
    {
        $medicine = Medicine::findOrFail($id);
        $medicine->name = $medicineData['name'];

        if (!$medicine->save()){
            throw new \DomainException('An error occurred while updating the medicine.');
        }

        return $medicine;
    }

    /**
     * @param int $id
     */
    public function destroy(int $id)
    {
        $medicine = Medicine::findOrFail($id);

        if (!$medicine->delete()){
            throw new \DomainException('Error while deleting');
        }
    }

}
