<?php

namespace App\Http\Services\Api\V1;

use App\Http\Resources\FilterCollection;
use App\Http\Resources\PatientCollection;
use App\Models\Medicine;
use App\Models\Patient;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class PatientService
{
    /**
     * @param array $data
     * @return AnonymousResourceCollection|mixed
     */
    public function create(array $data)
    {
        $patient = $this->findByNumberPatient($data['patient_id_number']);
        if ($patient->where('recovered', true)->exists() &&  $data['recovered'] == false){
            throw new \DomainException('This patient recovered.');
        }elseif ($patient->exists()){

            return $this->updateData($data);
        }

        return $this->save($data);
    }

    /**
     * @param int $id
     * @param array $data
     * @return AnonymousResourceCollection
     */
    public function update(int $id, array $data)
    {
        $patient = Patient::findOrFail($id);
        $patient->recovered = $data['recovered'];

        if (!$patient->save()){
            throw new \DomainException('An error occurred while updating patient the record.');
        }

        return PatientCollection::collection($patient);
    }

    /**
     * @param int $id
     */
    public function destroy(int $id)
    {
        $patient = Patient::findOrFail($id);

        if (!$patient->delete()){
            throw new \DomainException('An error occurred while delete patient.');
        }
    }

    /**
     * @param $data
     * @return mixed
     */
    private function save($data)
    {
        return DB::transaction(function () use ($data) {
            $patient = new Patient();
            $patient->patient_id_number = $data['patient_id_number'];
            $patient->doctor_id = $data['doctor_id'];
            $patient->full_name = $data['patient_full_name'];
            $patient->gender = $data['patient_gender'];
            $patient->recovered = $data['recovered'];
            $patient->save();
            $medicine = Medicine::findOrFail($data['medicines']);
            $patient->medicines()->saveMany($medicine);

            return PatientCollection::collection([$patient]);
        });
    }

    /**
     * @param $data
     * @return AnonymousResourceCollection
     */
    private function updateData($data)
    {
        $patient = Patient::where('patient_id_number', $data['patient_id_number'])->first();
        $patient->patient_id_number = $data['patient_id_number'];
        $patient->doctor_id = $data['doctor_id'];
        $patient->full_name = $data['patient_full_name'];
        $patient->gender = $data['patient_gender'];
        $patient->recovered = $data['recovered'];
        $patient->save();
        $patient->medicines()->sync($data['medicines']);

        return PatientCollection::collection($patient);

    }

    /**
     * @param int $patientNumber
     * @return Patient
     */
    private function findByNumberPatient(int $patientNumber)
    {
        return Patient::where('patient_id_number', $patientNumber);
    }

    /**
     * @param array $params
     * @param Patient $patient
     * @return AnonymousResourceCollection
     */
    public function searchByParams(array $params, Patient $patient)
    {
        $patient = $patient->newQuery();
        if (Arr::has($params, 'patient_name')){
            $patient->where('full_name','=', $params['patient_name']);
        }

        if (Arr::has($params, 'doctor_name')){
            $patient->whereHas('doctor', function ($query) use ($params){
                $query->where('full_name', '=', $params['doctor_name']);
            });
        }

        if (Arr::has($params, 'medicine_name')){
            Patient::with('medicines')->whereHas('medicines',  function($q) use($params){
                $q->where('name', '=', $params['medicine_name']);
            });
        }
        return FilterCollection::collection($patient->get());
    }
}
