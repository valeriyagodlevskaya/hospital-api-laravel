<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'full_name',
        'birth_day',
        'department'
    ];

    public function patients()
    {
        return $this->belongsToMany('App\Models\Patient');
    }
}
