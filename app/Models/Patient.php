<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'patient_id_number',
        'full_name',
        'gender',
        'recovered'
    ];

    public function doctor()
    {
        return $this->belongsTo('App\Models\Doctor');
    }

    public function medicines()
    {
        return $this->belongsToMany(Medicine::class, 'patient_medicine', 'patient_id', 'medicine_id');
    }

    public function med()
    {
        return $this->morphedByMany(Medicine::class, 'name');
    }
}
