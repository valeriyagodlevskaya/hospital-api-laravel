<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicineAndPatient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicines', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name');
        });

        Schema::create('patients', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->bigInteger('patient_id_number')->unsigned();
            $table->unsignedInteger('doctor_id');
            $table->string('full_name');
            $table->string('gender', 2);
            $table->boolean('recovered');

            $table->foreign('doctor_id')->references('id')->on('doctors')->onDelete('CASCADE');
        });

        Schema::create('patient_medicine', function (Blueprint $table) {
            $table->unsignedInteger('patient_id');
            $table->unsignedInteger('medicine_id');

            $table->foreign('patient_id')->references('id')->on('patients')->onDelete('CASCADE');
            $table->foreign('medicine_id')->references('id')->on('medicines')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_medicine');
        Schema::dropIfExists('medicines');
        Schema::dropIfExists('patients');
    }
}
