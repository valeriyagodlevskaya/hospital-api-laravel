<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Api\V1\DoctorController;
use \App\Http\Controllers\Api\V1\MedicineController;
use \App\Http\Controllers\Api\V1\PatientController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['as' => 'api.'], function () {
    Route::prefix('/v1/')->middleware('api')->group(function () {
        Route::post('doctors', [DoctorController::class, 'create']);
        Route::get('doctors/{id}/update', [DoctorController::class, 'update']);
        Route::get('doctors/{id}/delete', [DoctorController::class, 'delete']);
        Route::post('medicines', [MedicineController::class, 'create']);
        Route::get('medicines/{id}/update', [MedicineController::class, 'update']);
        Route::get('medicines/{id}/delete', [MedicineController::class, 'delete']);
        Route::post('patients', [PatientController::class, 'create']);
        Route::get('patients/{patient}/update', [PatientController::class, 'update']);
        Route::get('patients/{id}/delete', [PatientController::class, 'delete']);
        Route::get('patients', [PatientController::class, 'filter']);
    });
});

